use axum::{extract::{Path, Query}, headers::UserAgent, http::{HeaderMap, HeaderValue, StatusCode}, response::{IntoResponse, Response,}, Extension, Json, TypedHeader};
use serde::{Deserialize, Serialize};
use super::SharedData;

pub async fn this_aws_one_eventually () -> String {
    "Hello world!".to_owned()
}

pub async fn hello_world () -> String {
    "Hello world!".to_owned()
}

pub async fn get_string (body: String) -> String  {
    body
}

#[derive(Serialize, Deserialize, Debug)]
pub struct BodyJson {
    id: i32,
}

// #[derive(Serialize, Deserialize, Debug)]
// pub struct Response {
//     id: i32,
//     message: String,
// }

// pub async fn get_json (Json(body): Json<BodyJson>) -> Json<Response> {
//     Json(Response{
//         id: body.id,
//         message: "Success".to_owned(),
//     })
// }

pub async fn get_path_data(Path(id): Path<i32>) -> String {
    id.to_string()
}

#[derive(Serialize, Deserialize)]
pub struct QueryParams {
    id: i32,
    firstname: String,
    lastname: String,
}


pub async fn get_query_params(Query(query): Query<QueryParams>) -> Json<QueryParams> {
    Json(query)
}

pub async fn mirror_user_agent(TypedHeader(user_agent): TypedHeader<UserAgent>) -> String{
    user_agent.to_string()
}

pub async fn mirror_custom_header(headers: HeaderMap) ->  String {
    let message: &HeaderValue = headers.get("x-custom-header").unwrap();
    let message_val = message.to_str().unwrap().to_owned();
    message_val
}
#[derive(Serialize, Deserialize)]
pub struct CustomHeaders {
    accept: String,
    useragent: String,
    xcustomheader: String,
}

pub async fn mirror_custom_header_all(headers: HeaderMap) ->  Json<CustomHeaders> {
    let c_headers: CustomHeaders = CustomHeaders {
        accept: headers.get("Accept").unwrap().to_str().unwrap().to_owned(),
        useragent: headers.get("User-Agent").unwrap().to_str().unwrap().to_owned(),
        xcustomheader: headers.get("x-custom-header").unwrap().to_str().unwrap().to_owned(),
    };
    Json(c_headers)
}

pub async fn middleware_message(Extension(shared_data): Extension<SharedData>) -> String{
    shared_data.message
}


pub async fn always_errors() -> Result<(), StatusCode>{
    Err(StatusCode::BAD_REQUEST)
}

pub async fn return_201() -> Response {
    (StatusCode::CREATED,"201 return code",).into_response()
}