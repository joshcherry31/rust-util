mod main_routes;

use axum::{body::Body, http::Method, routing::{get, post}, Extension, Router};
use main_routes::{get_string, hello_world, get_path_data, get_query_params, mirror_user_agent, mirror_custom_header, mirror_custom_header_all, middleware_message, always_errors, return_201, this_aws_one_eventually};
use tower_http::cors::{CorsLayer, Any};

#[derive(Clone)]
pub struct SharedData {
    pub message: String,
}

pub fn create_routes() -> Router<Body> {

    let cors = CorsLayer::new()
        .allow_methods([Method::GET, Method::POST])
        .allow_origin(Any);


    let shared_data: SharedData = SharedData {
        message: "Testing shared data".to_owned(),
    };

    Router::new().route("/", get(hello_world))
        .route("/get_string", post(get_string))
        .route("/get_path_data/:id", get(get_path_data))
        .route("/get_query_params", get(get_query_params))
        .route("/mirror_user_agent", get(mirror_user_agent))
        .route("/mirror_custom_header", get(mirror_custom_header))
        .route("/mirror_custom_header_all", get(mirror_custom_header_all))
        .route("/middleware_message", get(middleware_message))
        .layer(cors)
        .layer(Extension(shared_data))
        .route("/always_errors", get(always_errors))
        .route("/return_201", post(return_201))
        .route("/this_aws_one_eventually", post(this_aws_one_eventually))
}
