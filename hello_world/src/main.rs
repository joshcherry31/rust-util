use hello_world::app_run;

#[tokio::main]
async fn main() {
    app_run().await
}

