use std::str::FromStr;
use axum::{Router, routing::get,response::Json,};
use std::net::{SocketAddr,IpAddr};
use serde::Serialize;
use serde_json::{Value, json};

#[derive(Serialize, Debug)]
struct RetData {
    name: String,
    age: u32,
}

async fn first_route() -> Json<Value> {
    let data: RetData = RetData {
        name: "Joshua Cherry".to_string(),
        age: 31,
    };
    let val: Value = json!(data);
    Json(val)
}

#[tokio::main]
async fn main() {
    let router = Router::new()
        .route("/basic_info", get(first_route));
    let address: &str = "127.0.0.1";
    let port: u16 = 8080;
    let addr: SocketAddr = SocketAddr::new(
        IpAddr::from_str(&address).unwrap(),
        port
    );
    axum::Server::bind(&addr)
        .serve(router.into_make_service())
        .await
        .unwrap();
}
