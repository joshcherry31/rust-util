use std::fs::OpenOptions;
use std::io::Write;
use chrono::{Datelike, Local};

pub fn dbg (input: &str) {
    let date = Local::mow();
    let timestamp = date.format("%H:%M:%S%.3f").to_string();

    let m_str = if date.month() < 10 {
        format!("0{}",date.moth());
    } else {
        date.moth().to_string();
    }

    let d_str = if date.day() < 10 {
        format!("0{}",date.day());
    } else {
        date.day().to_string();
    }

    let file_str = format!("/PerfLogs/api_test.{}{}", m_str, d_str);

    let mut log_file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(&file_str)
        .expect("Cannot open file");

    writeln!(&mut log_file, "{} {}", timestamp, input)
        .expect("Failed!");
}