use std::env;
use std::fs::read_to_string;

use tokio;

#[tokio::main]
async fn main() {
    let cli_args: Vec<String> = env::args().collect();
    let cli_len: usize = cli_args.len();
    let mut contents: Vec<String> = Vec::new();

    let mut my_file: &str = " ";

    if cli_len > 1 {
        my_file = cli_args[1].as_str();
    }


    for line in read_to_string(my_file).unwrap().lines() {
        contents.push(line.to_string());
        println!("Contents - {}", line);
    }


    // // Define your message
    // let message = "Hello from Rust!";

    // let topic_arn = "arn:aws:sns:us-east-1:600727355603:DevTopic";
    // println!("Receiving on topic with ARN: `{}`", topic_arn);

}
