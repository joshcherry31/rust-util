use anyhow::Result;
use opencv::{
    core::*, 
    highgui, 
    imgcodecs, 
    imgproc, 
    prelude::*, 
    videoio,
    objdetect::CascadeClassifier
};

fn main() -> Result<()>{

    let mut face_cascade = match CascadeClassifier::new("haarcascade_frontalface_default.xml") {
        Ok(fc) => fc,
        Err(e) => panic!("Failed to create cascade classifer - {}", e)
    };

    let img = match imgcodecs::imread("TestPic.jpg", imgcodecs::IMREAD_COLOR) {
        Ok(img) => img,
        Err(e) => panic!("Could not read image - {}", e),
    };

    if img.empty() {
        panic!("Failed to load picture"); 
    }
    let mut gray = Mat::default();
    // imgproc::cvt_color(&img, &mut gray, imgproc::COLOR_BGR2GRAY, 0, AlgorithmHint::ALGO_HINT_DEFAULT)?;

    let mut resized = Mat::zeros(800, 600, img.typ())?.to_mat()?;
    imgproc::resize(&img, &mut resized, Size::new(800,600), 0.0, 0.0, imgproc::INTER_LINEAR)?;
    highgui::named_window("Test one", highgui::WINDOW_AUTOSIZE)?;
    // highgui::imshow("Test one", &img)?;
    highgui::imshow("Test one", &resized)?;
    // let _ = highgui::wait_key(0);

    let cam_index = 0; // Change this if needed (e.g., 1 for an external webcam)
    let mut cam = videoio::VideoCapture::new(cam_index, videoio::CAP_ANY)?;
    
    if !videoio::VideoCapture::is_opened(&cam)? {
        panic!("Cannot open camera!");
    }

    let window = "Camera Feed";
    highgui::named_window(window, highgui::WINDOW_FULLSCREEN)?;
    let mut frame = Mat::default(); // This array will store the web-cam data

    loop {
        cam.read(&mut frame)?;

        if frame.size()?.width > 0 {
            highgui::imshow(window, &frame)?;
        }
        imgproc::cvt_color(&frame, &mut gray, imgproc::COLOR_BGR2GRAY, 0, AlgorithmHint::ALGO_HINT_DEFAULT)?;

        let mut faces = opencv::core::Vector::<opencv::core::Rect>::new();

        face_cascade.detect_multi_scale(&gray, &mut faces, 1.1, 2, 0, Size{width:0,height:0}, Size{width:0,height:0})?;

        for face in faces {
            
        }
        if highgui::wait_key(10)? == 27 { // Press 'Esc' to exit
            break;
        }
    }

    Ok(())
}
