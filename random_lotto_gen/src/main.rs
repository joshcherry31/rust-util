use rand::Rng;

fn main() {
    let secret_1 = rand::thread_rng().gen_range(1..=69);

    let mut secret_2 = rand::thread_rng().gen_range(1..=69);
    loop {
        if secret_2 != secret_1 {
            break;
        }
        secret_2 = rand::thread_rng().gen_range(1..=69);
    }
  
    let mut secret_3 = rand::thread_rng().gen_range(1..=69);
    loop {
        if secret_3 != secret_1 && secret_3 != secret_2{
            break;
        }
        secret_3 = rand::thread_rng().gen_range(1..=69);
    }
  
    let mut secret_4 = rand::thread_rng().gen_range(1..=69);
    loop {
        if secret_4 != secret_1 && secret_4 != secret_2 && secret_4 != secret_3{
            break;
        }
        secret_4 = rand::thread_rng().gen_range(1..=69);
    }
  
    let mut secret_5 = rand::thread_rng().gen_range(1..=69);
    loop {
        if secret_5 != secret_1 && secret_5 != secret_2 && secret_5 != secret_3 && secret_5 != secret_4{
            break;
        }
        secret_5 = rand::thread_rng().gen_range(1..=69);
    }
    let powerball = rand::thread_rng().gen_range(1..=26);

    println!("{secret_1} {secret_2} {secret_3} {secret_4} {secret_5} {powerball}");

}
